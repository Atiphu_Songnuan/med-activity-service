const jwt = require("jsonwebtoken");
const express = require("express");
const morgan = require("morgan");
morgan.token("date", function () {
  var p = new Date()
    .toString()
    .replace(/[A-Z]{3}\+/, "+")
    .split(/ /);
  return p[2] + "/" + p[1] + "/" + p[3] + ":" + p[4] + " " + p[5];
});

var path = require("path");
const mysql = require("mysql");
const https = require("https");
const cors = require("cors");
const logs = require("./createLogFile");
const fs = require("fs");
const querystring = require("querystring");
const { json } = require("body-parser");

const app = express();
app.use(morgan("combined"));
app.use(cors());
app.use(express.json());

// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, "access.log"), {
  flags: "a",
});

const pool = mysql.createPool({
  connectionLimit: 10,
  host: "172.29.70.129",
  user: "famz",
  password: "quiu7eedem",
  database: "vdovote",
});

// const pool = mysql.createPool({
//   connectionLimit: 10,
//   host: "localhost",
//   user: "root",
//   password: "",
//   database: "vdovote",
// });
// setup the logger
app.use(morgan("combined", { stream: accessLogStream }));

// https://medhr.medicine.psu.ac.th/app-api/v1/?/access

// *========== MEDQUIZ ==========
app.post("/api/quiz/login", (req, res) => {
  let body = req.body;
  let perid = body.perid;
  let pin = body.pin;
  let logindata = {
    id: perid,
    passwd: pin,
  };
  let jsondata = querystring.stringify(logindata);
  var options = {
    host: "medhr.medicine.psu.ac.th",
    path: "/app-api/v1/?/access",
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Content-Length": Buffer.byteLength(jsondata),
    },
  };

  const request = https
    .request(options, (response) => {
      let data = "";

      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        // console.log("Body: ", JSON.parse(data));
        res.send(JSON.parse(data));
      });
    })
    .on("error", (err) => {
      console.log("Error: ", err.message);
    });

  request.write(jsondata);
  request.end();
});

app.post("/api/quiz/refreshtoken", (req, res) => {
  let token = req.body.token;
  var options = {
    host: "medhr.medicine.psu.ac.th",
    path: "/app-api/v3/?/authentication/refresh",
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
    },
  };

  const request = https
    .request(options, (response) => {
      let data = "";

      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        // console.log("Body: ", JSON.parse(data));
        res.send(JSON.parse(data));
      });
    })
    .on("error", (err) => {
      console.log("Error: ", err.message);
    });
  request.end();
});

app.post("/api/quiz/answer", (req, res) => {
  let token = req.body.token;
  var options = {
    host: "medhr.medicine.psu.ac.th",
    path: "/app-api/v3/?/apis/quiz-answer",
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
    },
  };

  const request = https
    .request(options, (response) => {
      let data = "";

      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        // console.log("Body: ", JSON.parse(data));
        res.send(JSON.parse(data));
      });
    })
    .on("error", (err) => {
      console.log("Error: ", err.message);
    });

  request.end();

  // https
  //   .get(options, (resp) => {
  //     let data = "";
  //     // A chunk of data has been recieved.
  //     resp.on("data", (chunk) => {
  //       data += chunk;
  //     });

  //     // The whole response has been received. Print out the result.
  //     resp.on("end", () => {
  //       res.send(JSON.parse(data));
  //     });
  //   })
  //   .on("error", (err) => {
  //     console.log("Error: " + err.message);
  //   });
});

app.get("/api/quiz/questions", (req, res) => {
  var options = {
    host: "medhr.medicine.psu.ac.th",
    path:
      "/app-api/v3/?/apis/quiz/session/1/?fields=quiz_id,questionSeq,question&sort=questionSeq_asc",
    method: "GET",
    rejectUnauthorized: false,
  };

  const request = https
    .request(options, (response) => {
      let data = "";

      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        // console.log("Body: ", JSON.parse(data));
        res.send(JSON.parse(data));
      });
    })
    .on("error", (err) => {
      console.log("Error: ", err.message);
    });

  request.end();

  // https
  //   .get(options, (resp) => {
  //     let data = "";
  //     // A chunk of data has been recieved.
  //     resp.on("data", (chunk) => {
  //       data += chunk;
  //     });

  //     // The whole response has been received. Print out the result.
  //     resp.on("end", () => {
  //       res.send(JSON.parse(data));
  //     });
  //   })
  //   .on("error", (err) => {
  //     console.log("Error: " + err.message);
  //   });
});

app.post("/api/quiz/choices", (req, res) => {
  let questionid = req.body.questionid;
  var options = {
    host: "medhr.medicine.psu.ac.th",
    path:
      "/app-api/v3/?/apis/quiz/session/1/quiz_id/" +
      questionid +
      "/?fields=choiceSeq,choiceLabel&sort=questionSeq_asc",
    method: "GET",
    rejectUnauthorized: false,
  };

  const request = https
    .request(options, (response) => {
      let data = "";

      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        // console.log("Body: ", JSON.parse(data));
        res.send(JSON.parse(data));
      });
    })
    .on("error", (err) => {
      console.log("Error: ", err.message);
    });
  request.end();

  // https
  //   .get(options, (resp) => {
  //     let data = "";
  //     // A chunk of data has been recieved.
  //     resp.on("data", (chunk) => {
  //       data += chunk;
  //     });

  //     // The whole response has been received. Print out the result.
  //     resp.on("end", () => {
  //       res.send(JSON.parse(data));
  //     });
  //   })
  //   .on("error", (err) => {
  //     console.log("Error: " + err.message);
  //   });
});

app.post("/api/quiz/sendanswer", (req, res) => {
  let token = req.body.token;
  let answerbody = req.body.answerbody;
  let data = JSON.stringify(answerbody);

  let logdata = {
    date: new Date().toString(),
    token: token,
    data: answerbody,
  };
  logs.createLogFile("answer", logdata);

  var options = {
    host: "medhr.medicine.psu.ac.th",
    path: "/app-api/v3/?/apis/quiz-answer",
    method: "POST",
    headers: {
      Authorization: "Bearer " + token,
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(data),
    },
  };

  const request = https
    .request(options, (response) => {
      let data = "";

      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        // console.log("Body: ", JSON.parse(data));
        res.send(JSON.parse(data));
      });
    })
    .on("error", (err) => {
      console.log("Error: ", err.message);
    });

  request.write(data);
  request.end();

  // https
  //   .get(options, (resp) => {
  //     let data = "";
  //     // A chunk of data has been recieved.
  //     resp.on("data", (chunk) => {
  //       data += chunk;
  //     });

  //     // The whole response has been received. Print out the result.
  //     resp.on("end", () => {
  //       console.log(data);
  //       res.send(JSON.parse(data));
  //     });
  //   })
  //   .on("error", (err) => {
  //     console.log("Error: " + err.message);
  //   });
});
// *========== END-MEDQUIZ ==========

// *========== VDO Vote ==========
app.post("/api/vdovote/login", (req, res) => {
  let body = req.body;
  let perid = body.perid;
  let pin = body.pin;
  let logindata = {
    id: perid,
    passwd: pin,
  };
  let jsondata = querystring.stringify(logindata);
  var options = {
    host: "medhr.medicine.psu.ac.th",
    path: "/app-api/v1/?/access",
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Content-Length": Buffer.byteLength(jsondata),
    },
  };

  const request = https
    .request(options, (response) => {
      let data = "";

      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        // console.log("Body: ", JSON.parse(data));
        res.send(JSON.parse(data));
      });
    })
    .on("error", (err) => {
      console.log("Error: ", err.message);
    });

  request.write(jsondata);
  request.end();
});

app.post("/api/vdovote/refreshtoken", (req, res) => {
  let auth = req.headers.authorization;
  var options = {
    host: "medhr.medicine.psu.ac.th",
    path: "/app-api/v3/?/authentication/refresh",
    method: "GET",
    headers: {
      Authorization: auth,
    },
  };

  const request = https
    .request(options, (response) => {
      let data = "";

      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        // console.log("Body: ", JSON.parse(data));
        res.send(JSON.parse(data));
      });
    })
    .on("error", (err) => {
      console.log("Error: ", err.message);
    });
  request.end();
});

app.post("/api/vdovote/info", (req, res) => {
  let auth = req.headers.authorization;
  let token = auth.split(" ")[1];
  var options = {
    host: "medhr.medicine.psu.ac.th",
    path: "/app-api/v1/?/info",
    method: "GET",
    headers: {
      Authorization: auth,
    },
  };

  const request = https
    .request(options, (response) => {
      let data = "";

      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        // console.log("Body: ", JSON.parse(data));
        let resultJson = JSON.parse(data);
        console.log(resultJson);
        console.log(token);
        jwt.verify(token, "smartMedPsu!@2019", (err, decoded) => {
          if (err) {
            console.log(err);
          } else {
            resultJson["perid"] = decoded.id;
          }
        });
        res.send(resultJson);
      });
    })
    .on("error", (err) => {
      console.log("Error: ", err.message);
    });
  request.end();
});

app.post("/api/vdovote/checkvoted", (req, res) => {
  let auth = req.headers.authorization;
  let token = auth.split(" ")[1];
  jwt.verify(token, "smartMedPsu!@2019", (err, decoded) => {
    if (err) {
      console.log(err);
    } else {
      try {
        let values = [];
        values = [decoded.id];
        let sql = "SELECT * FROM vote WHERE perid = ?";
        pool.query(sql, values, (err, response) => {
          // console.log(response);
          let result = [];
          if (err) {
            result = {
              statusCode: 400,
              data: err,
            };
          } else {
            if (response.length != 0) {
              result = {
                statusCode: 200,
                data: response,
              };
            } else {
              result = {
                statusCode: 200,
                data: [],
              };
            }
          }
          res.send(result);
        });
      } catch (e) {
        console.log("Error:", e.stack);
      }
    }
  });
});

app.post("/api/vdovote/sendvote", (req, res) => {
  let votebody = req.body.votebody;
  try {
    let sql = "INSERT INTO vote(perid, vdo_no, vote_date) VALUES(?)";
    let values = [];
    values.push([votebody.perid, votebody.voteno, new Date()]);
    pool.query(sql, values, (err, response) => {
      // console.log(response);
      let result = [];
      if (err) {
        result = {
          statusCode: 400,
          data: err,
        };
      } else {
        result = {
          statusCode: 200,
          data: response,
        };
      }
      res.send(result);
    });
  } catch (e) {
    console.log("Error:", e.stack);
  }
});
// *========== END-VDO Vote ==========

const port = process.env.PORT || 5338;
app.listen(port, "0.0.0.0", () =>
  console.log("Listening on port " + port + "...")
);
